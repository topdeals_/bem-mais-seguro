$(document).ready(function(){  		
	$(window).scroll(function() {	
		if($(window).scrollTop() > 535){
			
			$('#bar').addClass('fixed');
		}
		else{
			$('#bar').removeClass('fixed');
		}
	});
	
	dimensions();
	
	$(window).load(function(){
		$('#loader').fadeOut(250);
	});
	
	$('.onlyNumber').keypress(function (e){
		var charCode = (e.which) ? e.which : e.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
	});
});

function dimensions(){
	heightWindow = $(window).height();	
	$('.mask').height(heightWindow);
	$('#loader').css({
		'height':heightWindow
	});
	marginWindow = ((heightWindow / 2) - ($('#loader div').height() / 2));
	$('#loader div').css({
		'margin-top':marginWindow
	});
	marginWindow = ((heightWindow / 2) - ($('.mask #return').height() / 2));
	$('.mask #return').css({
		'margin-top':marginWindow
	});
	//$('.part').height(heightWindow);	
};

function validate(formId){
	var valid = false;
	$(formId+' input[type="text"]').each(function(){
		if($(this).attr('name') != 'email'){
			if(!$.trim($(this).val())){
				$(this).css({
					'border-color':'#FF0000'
				})
				valid = true;
			}
			else{
				$(this).css({
					'border-color':'#80C342'
				})
				valid = false;
			}
		}
	});
	if(valid){			
		return false;
	}	
};

function anchor(box){
	$('body, html').animate({
		scrollTop: ($(box).offset().top - 67)
	}, 'slow');
};