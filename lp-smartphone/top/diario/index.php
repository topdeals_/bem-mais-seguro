<!DOCTYPE html>
<html lang='pt-br'>
<head>
	<meta charset='utf-8'/>	
	<meta name='title' content='Bem Mais Seguro'/>
	<meta name='description' content=''/>            
	<meta name='keywords' content=''/>
	<meta property='og:title' content='Bem Mais Seguro'/>
	<meta property='og:description' content=''/>
	<meta property='og:url' content=''/>
	<meta property='og:site_name' content='Bem Mais Seguro'/>
	<meta property='og:type' content='website'/>
	<meta property='og:image' content='images/share.png'/> 
	<link rel='image_src' href='images/share.png'/>
	<link rel='shortcut icon' type='image/x-icon' href='images/favicon.ico'/>		
	<link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,700' rel='stylesheet' type='text/css'>
	<link href='css/css.css' rel='stylesheet' type='text/css' media='all'/>	
	<script src='js/jquery-1.11.0.min.js'></script>			
	<script src='js/functions.js'></script>		
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-48005855-2', 'segurosbms.com.br');
	  ga('require', 'displayfeatures');  
	  ga('send', 'pageview');

	</script>
	<title>Bem Mais Seguro</title>
</head>
<body>		
	<!-- loader -->
	<div id='loader'>
		<div>
			<img src='images/logo.png' alt='logo'/>
			<p>CARREGANDO</p>
			<img src='images/loader.gif' alt='loader'/>
		</div>
	</div>
	<!-- /loader -->
	<!-- return -->
	<div class='mask'>		
		<div id='return'>		
			<b>MENSAGEM ENVIADA COM SUCESS0!</b>
			<p>EM BREVE ENTRAREMOS EM CONTATO!</p>		
		</div>
	</div>
	<!-- /return -->	
	<header>
		<div id='bar'>
			<div id='bgBar'></div>
			<div class='full'>
				<div id='baxFixed'>
					<a href='http://www.bemmaisseguro.com/' target='_blank'>
						<img src='images/logo.png' alt='logo'/>
					</a>
					<p>Seguro Celular</p>
					<span id='title'>
						Seu Smartphone <b></b> Seguro
					</span>
				</div>
			</div>
			<div id='banner'>
				<div class='full'>
					<h1>Você está protegendo o que mais gosta?</h1>
					<hr/>
					<h2>Seu celular quebrado vai te fazer falta</h2>
					<div class='box'>					
						<span>
							<p>
								<b>Seu celular protegido a partir das 24h</b>
							</p>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div id='form'>
			<div id='formContact'>
				<span class='top'>
					Proteja seu Smartphone
				</span>
				<form method='post' action=''>
					<fieldset>
						<span class='image'></span>
						<hr/>
						<p>É muito fácil e rápido</p>
						<input type='text' name='name' placeholder='Nome'/>
						<input type='text' name='phone' class='onlyNumber' maxlength='11' placeholder='Telefone'/>
						<input type='submit' name='send' onClick='return validate("#formContact form");'/>
					</fieldset>
				</form>
				<?php				
				if(isset($_POST['send'])){				
					if(PHP_OS == "Linux"){
						$line = "\n";
					}
					else if(PHP_OS == "WINNT"){
						$line = "\r\n";
					}

					$mailSend = 'contato@segurosbms.com.br';
					$recipient = 'contato@segurosbms.com.br';      					
					$email = ($_POST['email'])? $_POST['email'] : 'contato@segurosbms.com.br';
					$copy = 'doni.vieira@topdeals.com.br';
					//$copyHidden = 'rogerio.conti@topdeals.com.br';  
					$subject = 'Landing Page Smartphone - Contato';

					$emailContent = ($_POST['email'])? $_POST['email'] : 'não especificado!';
					$html = '
						<table align="center" cellpadding="20" cellspacing="0">
						  <tr>
							<td colspan="2" bgcolor="#d7d7d7"><font style="color: #504138; font: normal 26px Verdana">'.$subject.'</font></td>
						  </tr>
						  <tr>
							<td bgcolor="#efefef"><b style="color: #504138; font: bold 14px Verdana">Nome:</b></td>
							<td bgcolor="#efefef" style="color: #504138; font: normal 14px Verdana">'.$_POST['name'].'</td>
						  </tr>
						  <tr>
							<td bgcolor="#efefef"><b style="color: #504138; font: bold 14px Verdana">Telefone:</b></td>
							<td bgcolor="#efefef" style="color: #504138; font: normal 14px Verdana">'.$_POST['phone'].'</td>
						  </tr>						  
						</table>
					'; 
					$html = (utf8_decode($html));
					
					$headers = "MIME-Version: 1.1".$line;
					$headers .= "Content-type: text/html; charset=iso-8859-1".$line;
					$headers .= "From: ".$email.$line;
					$headers .= "Return-Path: " . $recipient . $line;      
					$headers .= "Cc: ".$copy.$line;      
					$headers .= "Bcc: ".$copyHidden.$line;      
					$headers .= "Reply-To: ".$email.$line;

					$send = mail($recipient, $subject, $html, $headers, "-r". $mailSend);      

					if(!$send){
						echo 'Ocorreu um erro!';					
					}
					else{												
						include('connect.php');						
						$sql = "INSERT INTO client (id, name, phone, email, date, page, notice) VALUES ('', '".$_POST['name']."', '".$_POST['phone']."', '".$emailContent."', '".date('Y-m-d')."', 'top smartphone', 'valor diário');";						
						mysql_query($sql) or die (mysql_error());
					?>
						<script>
							$('.mask').fadeIn(400).delay(2500).fadeOut(400);							
						</script>
					<?php
					}
				}				
				?>
			</div>
		</div>		
		<div class='title'>
			<div class='full'>
				O que o seguro cobre?
			</div>
		</div>
	</header>
	<section>
		<div id='icons'>
			<div class='full'>
				<div class='box'>
					<ul>
						<li id='theft'>
							<span></span>
							<div class='boxContent'>
								<p>Roubo ou Furto**</p>
								<b></b>
							</div>
							<div class='boxHidden'>
								<h2>Lorem ipsum dolor sit amet</h2>
								<p>consectetur adipiscing elit. Pellentesque dictum imperdiet augue sit amet mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam aliquet velit vel leo sollicitudin hendrerit. Etiam orci nisl, viverra eu urna aliquam, scelerisque iaculis eros. Nulla in gravida mi. Morbi sed euismod nisl, sit amet gravida urna. Nulla ac augue faucibus, vestibulum neque quis, congue sapien. Vivamus sodales augue quis est tincidunt cursus.</p>
							</div>
						</li>
						<li id='broken'>
							<span></span>
							<div class='boxContent'>
								<p>Quebra Acidental</p>
								<b></b>
							</div>
							<div class='boxHidden'>
								<h2>Lorem ipsum dolor sit amet</h2>
								<p>consectetur adipiscing elit. Pellentesque dictum imperdiet augue sit amet mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam aliquet velit vel leo sollicitudin hendrerit. Etiam orci nisl, viverra eu urna aliquam, scelerisque iaculis eros. Nulla in gravida mi. Morbi sed euismod nisl, sit amet gravida urna. Nulla ac augue faucibus, vestibulum neque quis, congue sapien. Vivamus sodales augue quis est tincidunt cursus.</p>
							</div>
						</li>
						<li id='liquid'>
							<span></span>
							<div class='boxContent'>
								<p>Queda de Líquidos</p>
								<b></b>
							</div>
							<div class='boxHidden'>
								<h2>Lorem ipsum dolor sit amet</h2>
								<p>consectetur adipiscing elit. Pellentesque dictum imperdiet augue sit amet mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam aliquet velit vel leo sollicitudin hendrerit. Etiam orci nisl, viverra eu urna aliquam, scelerisque iaculis eros. Nulla in gravida mi. Morbi sed euismod nisl, sit amet gravida urna. Nulla ac augue faucibus, vestibulum neque quis, congue sapien. Vivamus sodales augue quis est tincidunt cursus.</p>
							</div>
						</li>
						<li id='authorize'>
							<span></span>
							<div class='boxContent'>
								<p>Chamadas não autorizadas</p>
								<b></b>
							</div>
							<div class='boxHidden'>
								<h2>Lorem ipsum dolor sit amet</h2>
								<p>consectetur adipiscing elit. Pellentesque dictum imperdiet augue sit amet mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam aliquet velit vel leo sollicitudin hendrerit. Etiam orci nisl, viverra eu urna aliquam, scelerisque iaculis eros. Nulla in gravida mi. Morbi sed euismod nisl, sit amet gravida urna. Nulla ac augue faucibus, vestibulum neque quis, congue sapien. Vivamus sodales augue quis est tincidunt cursus.</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>		
		<div id='contact'>
			<div class='full'>
				<div class='boxContact'>
					<div class='box'>
						<span></span>
						<div>
							<p><b>•</b> Contratar Seguro</p>
							<p><b>•</b> Informações</p>
							<p><b>•</b> Dúvidas</p>
							<hr/>
							<h3>Ligue Agora</h3>
						</div>
					</div>
					<div class='text'>
						<p>Televendas <b>(11) 3003-0965</b></p>
						<span>
							Proteja aquilo que mais gosta
						</span>
					</div>
				</div>
			</div>
		</div>
		<div id='need'>
			<div class='full'>
				<div class='box'>
					<h3>Por que você precisa de um seguro para Smartphone?</h3>
					<hr/>
					<p><b>•</b> As vantagens começam pelo bolso. Um SmartPhone novo custa em média R$ 1.500,00 de acordo com o modelo.</p>
					<p><b>•</b> Você conta com cobertura de conserto de tela, em casos de quebra ou queda que impeçam o funcionamento do aparelho.</p>
					<p><b>•</b> O Brasil é o segundo país com o maior índice de roubo no mundo. Se você se juntar a esta estatística, não se preocupe. Você receberá um outro aparelho.***</p>
					<p><b>•</b> Temos os melhores preços do mercado.</p>
					<p><b>•</b> A grande maioria dos problemas com celulares são oriundos de pequenos acidentes. Não ache que só acontece com os outros.</p>
					<em>***Fonte: F-Secure - 06/2013</em>
				</div>
			</div>
		</div>
		<div id='question'>
			<div class='full'>
				<p>Ainda está com dúvida? <b>Veja as perguntas mais frequentes</b></p>
			</div>			
		</div>
		<ul id='boxReply'>
			<li id='reply1'  class='view'>
				<div class='darkGreen'>
					<div class='full'>						
						<a href='#' class='selected'></a>
						<h3>Por que preciso segurar meu celular?</h3>
					</div>					
				</div>
				<div class='full'>
					<span>
						Vamos a qualquer lugar com nossos Smartphones e isso significa que corremos o risco de ter nosso aparelho roubado ou danificado a qualquer momento. Nosso seguro garante a tranquilidade de saber que se o pior acontecer e você ficar sem seu aparelho.
					</span>		
				</div>
			</li>
			<li id='reply2'>
				<div class='lightGreen'>
					<div class='full'>						
						<a href='#'></a>
						<h3>Quanto custa o seguro para meu celular?</h3>
					</div>					
				</div>
				<div class='full'>
					<span>
						O seguro para o seu Smartphone dependerá do modelo, mas os preços variam entre R$ 5,99 a R$ 55,99 mensais.
					</span>	
				</div>
			</li>
			<li id='reply3'>
				<div class='darkGreen'>
					<div class='full'>						
						<a href='#'></a>
						<h3>A partir de quando meu celular/ smartphone estará segurado?</h3>
					</div>					
				</div>
				<div class='full'>
					<span>
						A partir das 24h da data de pagamento do seguro.
					</span>
				</div>
			</li>
			<li id='reply4'>
				<div class='lightGreen'>
					<div class='full'>						
						<a href='#'></a>
						<h3>Posso segurar meu celular seminovo?</h3>
					</div>					
				</div>
				<div class='full'>
					<span>
						Sim, você poderá segurar seu aparelho até 12 meses após a data de compra (conforme data da Nota Fiscal). Este é um dos nossos maiores diferenciais.
					</span>	
				</div>
			</li>
			<li id='reply5'>
				<div class='darkGreen'>
					<div class='full'>						
						<a href='#'></a>
						<h3>O que não está coberto?</h3>
					</div>					
				</div>
				<div class='full'>
					<span>
						Furto simples ou perda são eventos que não terão cobertura. Assim como danos causados exclusivamente à bateria ou carregador, mesmo decorrente de riscos cobertos. Leia com atenção as Condições Gerais e seu Bilhete de Seguro para conferir esses e todos os outros riscos excluídos.
					</span>	
				</div>					
			</li>
			<li id='reply6'>
				<div class='lightGreen'>
					<div class='full'>						
						<a href='#'></a>
						<h3>Há limite de indenizações?</h3>
					</div>					
				</div>
				<div class='full'>
					<span>
						No caso de Roubo ou Furto qualificado o segurado terá direto a apenas 1 troca no ano limitado ao valor do aparelho. Para as coberturas de dano acidental e queda de liquido a indenização será limitada ao valor do aparelho segurado.
					</span>	
				</div>					
			</li>
			<li id='reply7'>
				<div class='darkGreen'>
					<div class='full'>						
						<a href='#'></a>
						<h3>Se meu aparelho for roubado tenho que pagar franquia?</h3>
					</div>					
				</div>
				<div class='full'>
					<span>
						Sim, o valor da franquia varia de R$ 25,00 a R$ 1.000,00, dependendo do modelo do aparelho segurado.
					</span>
				</div>					
			</li>
			<li id='reply8'>
				<div class='lightGreen'>
					<div class='full'>						
						<a href='#'></a>
						<h3>Preciso fornecer Nota Fiscal de Compra para contratar o seguro?</h3>
					</div>					
				</div>
				<div class='full'>
					<span>
						Sim. Pedimos que você envie a nota fiscal de compra no momento da aquisição do seguro que contenha: data de compra, modelo, e preço. O upload de sua nota fiscal (escaneada ou uma foto legível) pode ser feito a qualquer momento na área “Meu Espaço” do nosso site.
					</span>
				</div>					
			</li>
			<li id='reply9'>
				<div class='darkGreen'>
					<div class='full'>						
						<a href='#'></a>
						<h3>Como acionar meu seguro?</h3>
					</div>					
				</div>
				<div class='full'>
					<span>
						1 . Você deve entrar em contato através dos telefones 3003 0964 (Grandes Centros e Capitais) / 0800 606 6306 (Demais Localidades) Atendimento das 08hs as 20hs, de Segunda a Sábado ou entrar em nosso site através do Chat com nossos especialistas de atendimento ao cliente e fazer a abertura da solicitação de indenização.<br/><br/>
						2 . Serão solicitados alguns documentos de acordo com o evento ocorrido segurado, confira nas condições gerais do produto e em seu bilhete de seguro o detalhamento dos documentos necessários para indenização.
					</span>		
				</div>
			</li>
		</ul>
		<div id='credit'>
			<div class='full'>	
				<div class='box'>	
					<h3>E por que confiar na <b>BemMaisSeguro.com</b>?</h3>
					<hr/>
					<ul>
						<li id='client'>
							<div>
								<span></span>
								<hr/>
								<p>Desde nosso lançamento em 2008, já seguramos <b>mais de 750.000 clientes pelo mundo</b></p>
							</div>
						</li>
						<li id='business'>
							<div>
								<span></span>
								<hr/>
								<p>Pertencemos a uma das <b>maiores empresas globais</b></p>
							</div>
						</li>
						<li id='contract'>
							<div>
								<span></span>
								<hr/>
								<p>Com toda essa experiência garantimos o melhor serviço. <b>Aproveite e faça o seu seguro agora!</b></p>
							</div>
						</li>
						<div class='clear'></div>
					</ul>
				</div>
			</div>
	</section>
	<footer>
		</div>
		<div class='full'>				
			<div id='barPromote'>					
				<a href='https://www.facebook.com/bemmaisseguro' target='_blank'>Quer estar por dentro das promoções? <p>Clique aqui e curta!</p></a>
			</div>
			<div class='boxContact'>
				<div class='box'>
					<span></span>
					<div>
						<p><b>•</b> Contratar Seguro</p>
						<p><b>•</b> Informações</p>
						<p><b>•</b> Dúvidas</p>
						<hr/>
						<h3>Ligue Agora</h3>
					</div>
				</div>
				<div class='text'>
					<p>Televendas <b>(11) 3003-0965</b></p>
					<span>
						Proteja aquilo que mais gosta
					</span>
				</div>
			</div>
			<div id='footerContent'>				
				<a href='http://www.bemmaisseguro.com/' target='_blank'>
					<img src='images/logo.png' alt='logo'/>
				</a>							
				<p>*Valor anual a partir de R$ 118,80, sendo valor do IOF de R$ 8,76. Consulte condições gerais do seguro no site.</p>
				<p>A Assurant Direta Corretora de Seguros Ltda., BemMaisSeguro.com, inscrita no CNPJ/MF sob o Nº 04.613.348/0001-05 é uma empresa especializada na venda de seguros pela internet. A BemMaisSeguro.com atua em estrita observância à legislação securitária estando registrada como corretora de seguros na Superintendência de Seguros Privados - SUSEP nº 10.2018459.0, e cadastrada nas principais seguradoras do país. Disque SUSEP: 0800 021 8484 - A comercialização de seguri é fiscalizada pela SUSEP. Acesse o site e saiba mais informações: www.susep.gov.br. O registro deste plano na SUSEP não implica por parte da Autarquia, incentivo ou recomendação à sua comercialização. Em nosso site, você encontrará um ambiente seguro, fácil e intuitivo para comprar o seguro mais adequado e os meios para esclarecer as suas dúvidas.</p>
			</div>
		</div>
	</footer>
</body>
</html>