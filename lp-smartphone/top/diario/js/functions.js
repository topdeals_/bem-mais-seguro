$(document).ready(function(){  			
	dimensions();
	
	// box content icons
	$('section #icons .box ul li .boxContent b').on('mouseover', function(){	
		var idBox = $(this).parent('div').parent('li');		
		var box = '#' + $(idBox).attr('id') + ' .boxHidden';
		$(box).fadeIn(350);
		console.log(box);		
	});
	$('section #icons .box ul li .boxContent b').on('mouseleave', function(){		
		var idBox = $(this).parent('div').parent('li');		
		var box = '#' + $(idBox).attr('id') + ' .boxHidden';
		$(box).fadeOut(350);
	});
	
	// block reply
	$('#boxReply li a').on('click', function(){	
		var idBox = $(this).parent('div').parent('div').parent('li');				
		var boxLink = '#' + $(idBox).attr('id') + ' .full a';
		var box = '#' + $(idBox).attr('id') + ' .full span';
		if($(box).css('display') != 'block'){		
			// default			
			$('#boxReply li .full span').slideUp(250);
			$('#boxReply li a').removeClass('selected');
			
			$(box).slideDown(250);
			$(boxLink).addClass('selected');
		}
		return false;
	});
	
	$('.onlyNumber').keypress(function (e){
		var charCode = (e.which) ? e.which : e.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
	});	
	
	$(window).load(function(){
		$('#loader').fadeOut(250);
	});	
	
	$(window).resize(function(){
		dimensions();
	});	
});

function dimensions(){
	heightWindow = $(window).height();	
	$('#form').height(heightWindow - 58);			
	$('.mask').height(heightWindow);
	$('#loader').css({
		'height':heightWindow
	});
	marginWindow = ((heightWindow / 2) - ($('#loader div').height() / 2));
	$('#loader div').css({
		'margin-top':marginWindow
	});
	marginWindow = ((heightWindow / 2) - ($('.mask #return').height() / 2));
	$('.mask #return').css({
		'margin-top':marginWindow
	});
};

function validate(formId){
	var valid = false;
	$(formId+' input[type="text"]').each(function(){
		if($(this).attr('name') != 'email'){
			if(!$.trim($(this).val())){
				$(this).css({
					'background-color':'#F30C0C',
					'color':'#FFFFFF'
				})
				valid = true;
			}
			else{
				$(this).css({
					'background-color':'#FFFFFF',
					'color':'#363636'
				})
				valid = false;
			}
		}
	});
	if(valid){			
		return false;
	}	
};