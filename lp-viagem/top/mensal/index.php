<!DOCTYPE html>
<html lang='pt-br'>
<head>
	<meta charset='utf-8'/>	
	<meta name='title' content='Bem Mais Seguro'/>
	<meta name='description' content=''/>            
	<meta name='keywords' content=''/>
	<meta property='og:title' content='Bem Mais Seguro'/>
	<meta property='og:description' content=''/>
	<meta property='og:url' content=''/>
	<meta property='og:site_name' content='Bem Mais Seguro'/>
	<meta property='og:type' content='website'/>
	<meta property='og:image' content='images/share.png'/> 
	<link rel='image_src' href='images/share.png'/>
	<link rel='shortcut icon' type='image/x-icon' href='images/favicon.ico'/>		
	<link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,700' rel='stylesheet' type='text/css'>
	<link href='css/css.css' rel='stylesheet' type='text/css' media='all'/>	
	<script src='js/jquery-1.11.0.min.js'></script>			
	<script src='js/functions.js'></script>		
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-48005855-2', 'segurosbms.com.br');
	  ga('require', 'displayfeatures');  
	  ga('send', 'pageview');

	</script>
	<title>Bem Mais Seguro</title>
</head>
<body>		
	<!-- loader -->
	<div id='loader'>
		<div>
			<img src='images/logo.png' alt='logo'/>
			<p>CARREGANDO</p>
			<img src='images/loader.gif' alt='loader'/>
		</div>
	</div>
	<!-- /loader -->
	<!-- return -->
	<div class='mask'>		
		<div id='return'>		
			<b>MENSAGEM ENVIADA COM SUCESS0!</b>
			<p>EM BREVE ENTRAREMOS EM CONTATO!</p>		
		</div>
	</div>
	<!-- /return -->	
	<header>
		<div id='bar'>
			<div id='bgBar'></div>
			<div class='full'>
				<div id='baxFixed'>
					<a href='http://www.bemmaisseguro.com/' target='_blank'>
						<img src='images/logo.png' alt='logo'/>
					</a>
					<p>Seguro Viagem</p>
					<span id='title'>
						Sua viagem <b></b> protegida
					</span>
				</div>
			</div>
			<div id='banner'>
				<div class='full'>
					<h1>Você está protegendo o que mais gosta?</h1>
					<hr/>
					<h2>Torne a sua viagem mais tranquila e segura.</h2>
					<div class='box'>					
						<span>
							<p>
								<b>Serviço disponível durante 24 horas por dia.</b>
							</p>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div id='form'>
			<div id='formContact'>
				<span class='top'>
					Proteja sua Viagem
				</span>
				<form method='post' action=''>
					<fieldset>
						<span class='image'></span>
						<hr/>
						<p>É muito fácil e rápido</p>
						<input type='text' name='name' placeholder='Nome'/>
						<input type='text' name='phone' class='onlyNumber' maxlength='11' placeholder='Telefone'/>
						<input type='submit' name='send' onClick='return validate("#formContact form");'/>
						<em>*valor total a partir de R$ 101,00</em>
					</fieldset>
					<hr class='line'/>
					<div class='box'>
						<p>Desconto de <b>15%</b> para crianças de até 12 anos acompanhadas de um adulto.</p>
					</div>
				</form>
				<?php				
				if(isset($_POST['send'])){				
					if(PHP_OS == "Linux"){
						$line = "\n";
					}
					else if(PHP_OS == "WINNT"){
						$line = "\r\n";
					}

					$mailSend = 'contato@segurosbms.com.br';
					$recipient = 'contato@segurosbms.com.br';      					
					$email = ($_POST['email'])? $_POST['email'] : 'contato@segurosbms.com.br';
					$copy = 'doni.vieira@topdeals.com.br';
					//$copyHidden = 'rogerio.conti@topdeals.com.br';  
					$subject = 'Landing Page Viagem - Contato';

					$emailContent = ($_POST['email'])? $_POST['email'] : 'não especificado!';
					$html = '
						<table align="center" cellpadding="20" cellspacing="0">
						  <tr>
							<td colspan="2" bgcolor="#d7d7d7"><font style="color: #504138; font: normal 26px Verdana">'.$subject.'</font></td>
						  </tr>
						  <tr>
							<td bgcolor="#efefef"><b style="color: #504138; font: bold 14px Verdana">Nome:</b></td>
							<td bgcolor="#efefef" style="color: #504138; font: normal 14px Verdana">'.$_POST['name'].'</td>
						  </tr>
						  <tr>
							<td bgcolor="#efefef"><b style="color: #504138; font: bold 14px Verdana">Telefone:</b></td>
							<td bgcolor="#efefef" style="color: #504138; font: normal 14px Verdana">'.$_POST['phone'].'</td>
						  </tr>						  
						</table>
					'; 
					$html = (utf8_decode($html));
					
					$headers = "MIME-Version: 1.1".$line;
					$headers .= "Content-type: text/html; charset=iso-8859-1".$line;
					$headers .= "From: ".$email.$line;
					$headers .= "Return-Path: " . $recipient . $line;      
					$headers .= "Cc: ".$copy.$line;      
					$headers .= "Bcc: ".$copyHidden.$line;      
					$headers .= "Reply-To: ".$email.$line;

					$send = mail($recipient, $subject, $html, $headers, "-r". $mailSend);      

					if(!$send){
						echo 'Ocorreu um erro!';					
					}
					else{												
						include('connect.php');						
						$sql = "INSERT INTO client (id, name, phone, email, date, page, notice) VALUES ('', '".$_POST['name']."', '".$_POST['phone']."', '".$emailContent."', '".date('Y-m-d')."', 'top viagem', 'valor mensal');";						
						mysql_query($sql) or die (mysql_error());
					?>
						<script>
							$('.mask').fadeIn(400).delay(2500).fadeOut(400);							
						</script>
					<?php
					}
				}				
				?>
			</div>
		</div>		
		<div class='title'>
			<div class='full'>
				O que o seguro cobre?
			</div>
		</div>
	</header>
	<section>
		<div id='icons'>
			<div class='full'>
				<div class='box'>
					<ul>
						<li id='theft'>
							<span></span>
							<div class='boxContent'>
								<p>Assistência Médica</p>
								<b></b>
							</div>
							<div class='boxHidden'>
								<h2>Lorem ipsum dolor sit amet</h2>
								<p>consectetur adipiscing elit. Pellentesque dictum imperdiet augue sit amet mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam aliquet velit vel leo sollicitudin hendrerit. Etiam orci nisl, viverra eu urna aliquam, scelerisque iaculis eros. Nulla in gravida mi. Morbi sed euismod nisl, sit amet gravida urna. Nulla ac augue faucibus, vestibulum neque quis, congue sapien. Vivamus sodales augue quis est tincidunt cursus.</p>
							</div>
						</li>
						<li id='broken'>
							<span></span>
							<div class='boxContent'>
								<p>Cancelamento de Viagem**</p>
								<b></b>
							</div>
							<div class='boxHidden'>
								<h2>Lorem ipsum dolor sit amet</h2>
								<p>consectetur adipiscing elit. Pellentesque dictum imperdiet augue sit amet mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam aliquet velit vel leo sollicitudin hendrerit. Etiam orci nisl, viverra eu urna aliquam, scelerisque iaculis eros. Nulla in gravida mi. Morbi sed euismod nisl, sit amet gravida urna. Nulla ac augue faucibus, vestibulum neque quis, congue sapien. Vivamus sodales augue quis est tincidunt cursus.</p>
							</div>
						</li>
						<li id='liquid'>
							<span></span>
							<div class='boxContent'>
								<p>Extravio de Bagagem</p>
								<b></b>
							</div>
							<div class='boxHidden'>
								<h2>Lorem ipsum dolor sit amet</h2>
								<p>consectetur adipiscing elit. Pellentesque dictum imperdiet augue sit amet mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam aliquet velit vel leo sollicitudin hendrerit. Etiam orci nisl, viverra eu urna aliquam, scelerisque iaculis eros. Nulla in gravida mi. Morbi sed euismod nisl, sit amet gravida urna. Nulla ac augue faucibus, vestibulum neque quis, congue sapien. Vivamus sodales augue quis est tincidunt cursus.</p>
							</div>
						</li>
						<li id='authorize'>
							<span></span>
							<div class='boxContent'>
								<p>Seguro de Acidentes Pessoais</p>
								<b></b>
							</div>
							<div class='boxHidden'>
								<h2>Lorem ipsum dolor sit amet</h2>
								<p>consectetur adipiscing elit. Pellentesque dictum imperdiet augue sit amet mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam aliquet velit vel leo sollicitudin hendrerit. Etiam orci nisl, viverra eu urna aliquam, scelerisque iaculis eros. Nulla in gravida mi. Morbi sed euismod nisl, sit amet gravida urna. Nulla ac augue faucibus, vestibulum neque quis, congue sapien. Vivamus sodales augue quis est tincidunt cursus.</p>
							</div>
						</li>
					</ul>
					<em>**Cancelamento válido apenas em caso de morte ou internação por acidente ou doença de parente de primeiro grau, internação ou morte do próprio cliente ou convocação judicial.</em>
				</div>
			</div>
		</div>		
		<div id='contact'>
			<div class='full'>
				<div class='boxContact'>
					<div class='box'>
						<span></span>
						<div>
							<p><b>•</b> Contratar Seguro</p>
							<p><b>•</b> Informações</p>
							<p><b>•</b> Dúvidas</p>
							<hr/>
							<h3>Ligue Agora</h3>
						</div>
					</div>
					<div class='text'>
						<p>Televendas <b>(11) 3003-0965</b></p>
						<span>
							Proteja aquilo que mais gosta
						</span>
					</div>
				</div>
			</div>
		</div>
		<div id='need'>
			<div class='full'>
				<div class='box'>
					<h3>Por que você precisa de um seguro para a sua Viagem?</h3>
					<hr/>
					<p>É essencial você garantir uma assistência médica em qualquer lugar do mundo, além do custo, um atendimento médico deve ser bem direcionado para o seu conforto e segurança. O seguro inclui também um serviço de localização de bagagens, ou seja, suas malas sempre estarão em boas mãos. </p>
					<p>Além disso, caso precise tirar dúvidas ou obter qualquer informação sobre seu seguro durante a viagem, o atendimento será realizado em português através de uma ligação gratuita em qualquer lugar do mundo.</p>					
				</div>
			</div>
		</div>
		<div id='question'>
			<div class='full'>
				<p>Ainda está com dúvida? <b>Veja as perguntas mais frequentes</b></p>
			</div>			
		</div>
		<ul id='boxReply'>
			<li id='reply1'  class='view'>
				<div class='darkGreen'>
					<div class='full'>						
						<a href='#' class='selected'></a>
						<h3>O que é e para que serve o Seguro Viagem?</h3>
					</div>					
				</div>
				<div class='full'>
					<span>
						O Seguro Viagem é um conjunto de assistências para tornar sua viagem mais tranquila. Os serviços estão disponíveis 24 horas por dia e você pode acioná-los com um simples telefonema. O atendimento é feito em português e a solução para seu problema, seja uma simples indisposição ou um evento mais sério, será dada o mais breve possível e acompanhada até o final.
					</span>		
				</div>
			</li>
			<li id='reply2'>
				<div class='lightGreen'>
					<div class='full'>						
						<a href='#'></a>
						<h3>Por que devo adquirir o Seguro Viagem?</h3>
					</div>					
				</div>
				<div class='full'>
					<span>
						Quando estamos fora de nosso ambiente, seja em outro país ou mesmo outra cidade, pequenos problemas podem se transformar em situações mais complicadas. O Seguro Viagem ajuda você a resolver essas questões para aproveitar ao máximo a sua viagem. Considere também que uma simples consulta médica na Europa custa, em média, € 100,00 e nos Estados Unidos, US$ 200,00. Adquirindo o Seguro Viagem, você não precisa se preocupar com esses custos, pois, respeitadas as condições do produto, esses custos serão arcados pela assistência contratada. Assim, com um pequeno investimento, você passa a ter ao seu lado a tranquilidade da proteção 24 horas por dia, durante toda a sua viagem.
					</span>	
				</div>
			</li>
			<li id='reply3'>
				<div class='darkGreen'>
					<div class='full'>						
						<a href='#'></a>
						<h3>O que é o Tratado de Schengen?</h3>
					</div>					
				</div>
				<div class='full'>
					<span>
						O Tratado de Schengen é um acordo assinado entre países da Comunidade Européia (Bélgica, Dinamarca, Alemanha, Finlândia, França, Grécia, Islândia, Itália, Luxemburgo, Holanda, Noruega, Áustria, Portugal, Espanha e Suécia) que estabelece a obrigatoriedade de que os turistas visitando esses países comprovem possuir uma Assistência a Viagens com valor mínimo de € 30.000 para garantir assistência médica por doença ou acidente.
					</span>
				</div>
			</li>
			<li id='reply4'>
				<div class='lightGreen'>
					<div class='full'>						
						<a href='#'></a>
						<h3>Como posso pagar pelo meu Seguro Viagem?</h3>
					</div>					
				</div>
				<div class='full'>
					<span>
						O Seguro Viagem pode ser pago com seus cartões de crédito VISA, American Express, Mastercard, Diners, Aura e Hipercard.
					</span>	
				</div>
			</li>
			<li id='reply5'>
				<div class='darkGreen'>
					<div class='full'>						
						<a href='#'></a>
						<h3>Não recebi o certificado no meu e-mail. O que eu faço?</h3>
					</div>					
				</div>
				<div class='full'>
					<span>
						Caso o seu e-mail possua algum tipo de bloqueador anti-spam, certifique-se de desativá-lo para receber seu certificado. Se mesmo assim você não receber seu certificado, entre em contato com a nossa central de atendimento, solicitando o reenvio do certificado. Colocar o número da central
					</span>	
				</div>					
			</li>
			<li id='reply6'>
				<div class='lightGreen'>
					<div class='full'>						
						<a href='#'></a>
						<h3>O que devo fazer se precisar de assistência médica durante minha viagem?</h3>
					</div>					
				</div>
				<div class='full'>
					<span>
						Em caso de doença ou acidente, antes de tomar qualquer medida, o Cliente deve estar de posse do seu Certificado e:<br/><br/>
						Entrar em contato imediatamente com a Central de Atendimento, caracterizando a ocorrência e fornecendo todas as informações necessárias para a localização dos prestadores de serviço da assistência solicitada;<br/><br/>
						Informar corretamente e com clareza, todos os dados constantes do seu Certificado;<br/><br/>
						Seguir as instruções e tomar as medidas necessárias e possíveis para restringir os efeitos da ocorrência ou impedir o agravamento de suas consequências;<br/><br/>
						Satisfazer, sempre que necessário, os pedidos de informação e documentos solicitados e remeter-lhe prontamente todos os avisos, originais de convocações ou citações que receber, para o cumprimento das formalidades necessárias.
					</span>	
				</div>					
			</li>
			<li id='reply7'>
				<div class='darkGreen'>
					<div class='full'>						
						<a href='#'></a>
						<h3>O que devo fazer se minha bagagem for roubada?</h3>
					</div>					
				</div>
				<div class='full'>
					<span>
						Se sua bagagem for extraviada enquanto entregue para transporte a uma companhia aérea regular, você estará garantido desde que:<br/><br/>
						O extravio tenha acontecido em voos nacionais e internacionais, incluindo voos de regresso. Estará coberto também o extravio em voos realizados entre duas cidades, mesmo que dentro do mesmo país;<br/><br/>
						Tenha havido o desaparecimento total de mala ou volume, observando-se ainda as cláusulas de exclusão de cobertura da apólice;<br/><br/>
						O extravio tenha se dado no período entre a entrega da bagagem ao pessoal autorizado da Companhia Aérea para embarque e o momento da devolução ao passageiro, no final da viagem;<br/><br/>
						O extravio seja notificado à Companhia Aérea imediatamente, antes de abandonar o local de entrega de bagagem no aeroporto de destino, através do preenchimento do formulário P.I.R. (Property Irregularity Report); •  Nós tenhamos sidos informados sobre o ocorrido imediatamente após a reclamação com a companhia aérea, de dentro do aeroporto.
					</span>
				</div>					
			</li>			
		</ul>
		<div id='credit'>
			<div class='full'>	
				<div class='box'>	
					<h3>E por que confiar na <b>BemMaisSeguro.com</b>?</h3>
					<hr/>
					<ul>
						<li id='client'>
							<div>
								<span></span>
								<hr/>
								<p>Desde nosso lançamento em 2008, já seguramos <b>mais de 750.000 clientes pelo mundo</b></p>
							</div>
						</li>
						<li id='business'>
							<div>
								<span></span>
								<hr/>
								<p>Pertencemos a uma das <b>maiores empresas globais</b></p>
							</div>
						</li>
						<li id='contract'>
							<div>
								<span></span>
								<hr/>
								<p>Com toda essa experiência garantimos o melhor serviço. <b>Aproveite e faça o seu seguro agora!</b></p>
							</div>
						</li>
						<div class='clear'></div>
					</ul>
				</div>
			</div>
	</section>
	<footer>
		</div>
		<div class='full'>				
			<div id='barPromote'>					
				<a href='https://www.facebook.com/bemmaisseguro' target='_blank'>Quer estar por dentro das promoções? <p>Clique aqui e curta!</p></a>
			</div>
			<div class='boxContact'>
				<div class='box'>
					<span></span>
					<div>
						<p><b>•</b> Contratar Seguro</p>
						<p><b>•</b> Informações</p>
						<p><b>•</b> Dúvidas</p>
						<hr/>
						<h3>Ligue Agora</h3>
					</div>
				</div>
				<div class='text'>
					<p>Televendas <b>(11) 3003-0965</b></p>
					<span>
						Proteja aquilo que mais gosta
					</span>
				</div>
			</div>
			<div id='footerContent'>				
				<a href='http://www.bemmaisseguro.com/' target='_blank'>
					<img src='images/logo.png' alt='logo'/>
				</a>							
				<p>*Valor anual a partir de R$ 118,80, sendo valor do IOF de R$ 8,76. Consulte condições gerais do seguro no site.</p>
				<p>A Assurant Direta Corretora de Seguros Ltda., BemMaisSeguro.com, inscrita no CNPJ/MF sob o Nº 04.613.348/0001-05 é uma empresa especializada na venda de seguros pela internet. A BemMaisSeguro.com atua em estrita observância à legislação securitária estando registrada como corretora de seguros na Superintendência de Seguros Privados - SUSEP nº 10.2018459.0, e cadastrada nas principais seguradoras do país. Disque SUSEP: 0800 021 8484 - A comercialização de seguri é fiscalizada pela SUSEP. Acesse o site e saiba mais informações: www.susep.gov.br. O registro deste plano na SUSEP não implica por parte da Autarquia, incentivo ou recomendação à sua comercialização. Em nosso site, você encontrará um ambiente seguro, fácil e intuitivo para comprar o seguro mais adequado e os meios para esclarecer as suas dúvidas.</p>
			</div>
		</div>
	</footer>
</body>
</html>