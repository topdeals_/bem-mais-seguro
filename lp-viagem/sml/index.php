<!DOCTYPE html>
<html lang='pt-br'>
<head>
	<meta charset='utf-8'/>	
	<meta name='title' content='Bem Mais Seguro'/>
	<meta name='description' content=''/>            
	<meta name='keywords' content=''/>
	<meta property='og:title' content='Bem Mais Seguro'/>
	<meta property='og:description' content=''/>
	<meta property='og:url' content=''/>
	<meta property='og:site_name' content='Bem Mais Seguro'/>
	<meta property='og:type' content='website'/>
	<meta property='og:image' content='images/share.png'/> 
	<link rel='image_src' href='images/share.png'/>
	<link rel='shortcut icon' type='image/x-icon' href='images/favicon.ico'/>		
	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
	<link href='css/css.css' rel='stylesheet' type='text/css' media='all'/>	
	<script src='js/jquery-1.11.0.min.js'></script>			
	<script src='js/functions.js'></script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-48005855-2', 'segurosbms.com.br');
	  ga('require', 'displayfeatures');  
	  ga('send', 'pageview');

	</script>	
	<title>Bem Mais Seguro</title>
</head>
<body>	
	<!-- loader -->
	<div id='loader'>
		<div>
			<img src='images/logo.png' alt='logo'/>
			<p>CARREGANDO</p>
			<img src='images/loader.gif' alt='loader'/>
		</div>
	</div>
	<!-- /loader -->
	<!-- return -->
	<div class='mask'>		
		<div id='return'>		
			<b>MENSAGEM ENVIADA COM SUCESS0!</b>
			<p>EM BREVE ENTRAREMOS EM CONTATO!</p>		
		</div>
	</div>
	<!-- /return -->
	<header>
		<div class='full'>
			<a href='http://bemmaisseguro.com/' target='_blank' class='logo'></a>
			<p>Mais de 750.000 clientes pelo mundo</p>
		</div>
	</header>
	<div id='cover'>
		<div class='full'>
			<h1>
				Seguro Viagem.<br/>
				A proteção ideal<br/> para as férias que<br/> você tanto sonhou.
			</h1>
			<div class='buttonWhite' id='bt1' onClick='anchor("#broken");'></div>
		</div>
	</div>
	<div id='bgBar'>
		<div id='bar'>
			<div class='full'>
				<p>
					<b>Faça uma cotação.</b>
					Nós ligamos para você.
				</p>
				<form name='' id='formTop' method='post' action=''>
					<fieldset>
						<input type='text' name='name' placeholder='nome'/>
						<input type='text' name='phone' class='onlyNumber' placeholder='telefone'/>
						<input type='submit' name='sendTop' value='enviar' onClick='return validate("#formTop");'/>
					</fieldset>
				</form>
				<a href='http://bemmaisseguro.com/' target='_blank'>ou acesse o site</a>
			</div>
		</div>
	</div>
	<div class='part' id='broken'>
		<div class='full'>
			<h2>Assistência Médica<br/>e Odontológica</h2>			
			<p>Se você adoecer ou ocorrer qualquer tipo de<br/> acidente durante a sua viagem, seja nacional<br/>  ou internacional, você estará protegido por<br/>  uma cobertura total de até € 50.000.</p>
			<div class='buttonWhite' id='bt2' onClick='anchor("#liquid");'></div>
		</div>
	</div>
	<div class='part' id='liquid'>
		<div class='full'>
			<h2>Extravio de Bagagem</h2>
			<p>Em caso de extravio da sua bagagem pela<br/> companhia aérea, uma cobertura de até<br/> R$3.000 vai garantir a sua calma.</p>
			<div class='buttonGreen' id='bt3' onClick='anchor("#theft");'></div>
		</div>
	</div>
	<div class='part' id='theft'>
		<div class='full'>
			<h2>Condições especiais<br/> para crianças</h2>			
			<p>Crianças de até 12 anos tem 15% de desconto<br/> no seguro, quando acompanhadas de um adulto.</p>
			<div class='buttonWhite' id='bt4' onClick='anchor("#client");'></div>
		</div>
	</div>
	<div class='part' id='client'>
		<div class='full'>
			<h2>Centenas de milhares de clientes satisfeitos</h2>			
			<p>Desde nosso lançamento, em 2008, seguramos mais de 750.000 clientes no<br/> Reino Unido e nos Estados Unidos. Este reconhecimento já nos rendeu diversos<br/> prêmios e, agora, é a sua vez de poder aproveitar todas as nossas facilidades.</p>
			<div class='buttonGreen' id='bt5' onClick='anchor("#simplicity");'></div>
		</div>
	</div>
	<div class='part' id='simplicity'>
		<div class='full'>
			<h2>Simplicidade</h2>	
			<p>Acionar o seguro é muito simples.<br/> Basta ligar para a central de atendimento e,<br/> em poucos passos, você saberá exatamente<br/> o que tem que fazer.</p>
			<div class='buttonGreen' id='bt6' onClick='anchor("#contact");'></div>
		</div>
	</div>
	<footer>	
		<div id='contact'>
			<div class='full'>
				<h2>Tudo por um preço compatível com a sua viagem.</h2>
				<p>
					<b>Faça uma cotação.</b>
					Nós ligamos para você.
				</p>
				<form name='' id='formBottom' method='post' action=''>
					<fieldset>
						<input type='text' name='name' placeholder='nome'/>
						<input type='text' name='phone' class='onlyNumber' placeholder='telefone'/>
						<input type='text' name='email' placeholder='email (opcional)'/>
						<input type='submit' name='sendBottom' value='enviar' onClick='return validate("#formBottom");'/>
					</fieldset>
				</form>
				<?php
				if((isset($_POST['sendTop'])) OR (isset($_POST['sendBottom']))){				
					if(PHP_OS == "Linux"){
						$line = "\n";
					}
					else if(PHP_OS == "WINNT"){
						$line = "\r\n";
					}
					
					$mailSend = 'contato@segurosbms.com.br';
					$recipient = 'contato@segurosbms.com.br';      					
					$email = ($_POST['email'])? $_POST['email'] : 'contato@segurosbms.com.br';
					//$copy = 'doni.vieira@topdeals.com.br';
					//$copyHidden = 'rogerio.conti@topdeals.com.br';  
					$subject = 'Fale Conosco';

					$emailContent = ($_POST['email'])? $_POST['email'] : 'não especificado!';
					$html = '
						<table align="center" cellpadding="20" cellspacing="0">
						  <tr>
							<td colspan="2" bgcolor="#d7d7d7"><font style="color: #504138; font: normal 26px Verdana">'.$subject.'</font></td>
						  </tr>
						  <tr>
							<td bgcolor="#efefef"><b style="color: #504138; font: bold 14px Verdana">Nome:</b></td>
							<td bgcolor="#efefef" style="color: #504138; font: normal 14px Verdana">'.$_POST['name'].'</td>
						  </tr>
						  <tr>
							<td bgcolor="#efefef"><b style="color: #504138; font: bold 14px Verdana">Telefone:</b></td>
							<td bgcolor="#efefef" style="color: #504138; font: normal 14px Verdana">'.$_POST['phone'].'</td>
						  </tr>
						  <tr>
							<td bgcolor="#f5f5f5"><b style="color: #504138; font: bold 14px Verdana">E-mail:</b></td>
							<td bgcolor="#f5f5f5" style="color: #504138; font: normal 14px Verdana">'.$emailContent.'</td>
						  </tr>							  
						</table>
					'; 
					$html = (utf8_decode($html));
					
					$headers = "MIME-Version: 1.1".$line;
					$headers .= "Content-type: text/html; charset=iso-8859-1".$line;
					$headers .= "From: ".$email.$line;
					$headers .= "Return-Path: " . $recipient . $line;      
					$headers .= "Cc: ".$copy.$line;      
					$headers .= "Bcc: ".$copyHidden.$line;      
					$headers .= "Reply-To: ".$email.$line;

					$send = mail($recipient, $subject, $html, $headers, "-r". $mailSend);      

					if(!$send){
						echo 'Ocorreu um erro!';					
					}
					else{						
						include('connect.php');
						$sql = "INSERT INTO client (id, name, phone, email, date, page, notice) VALUES ('', '".$_POST['name']."', '".$_POST['phone']."', '".$emailContent."', '".date('Y-m-d')."', 'vml viagem', '');";
						/* flag lp 
							1 = smartphone(lp do cliente)
							2 = smartphone(lp da casa)
						*/
						mysql_query($sql) or die (mysql_error());
					?>
						<script>
							$('.mask').fadeIn(400).delay(2500).fadeOut(400);							
						</script>
					<?php
					}
				}
				?>
				<span>
					ou ligue para <b>3003-0965</b>
					<p>Capitais e regiões metropolitanas</p>
				</span>
				<a href='http://bemmaisseguro.com/' target='_blank' class='logo'></a>
			</div>
		</div>	
		<div id='info'>
			<div class='full'>
				<p>* Consulte no site www.bemmaisseguro.com.br as condições Gerais ou o Resumo do Seguro para a lista completa de coberturas e exclusões.</p>
				<p>A Assurant Direta Corretora de Seguros LTDA., BemMaisSeguro.com, inscrita no CNPJ/MF sob o Nº 04.613.348/0001-05 é uma empresa especializada na venda de seguros pela internet. A BemMaisSeguro.com atua em estrita observância à legislação securitária estando registrada como corretora de seguros na Superintendência de Seguros Privados – SUSEP nº 10.2018459.0, e cadastrada nas principais seguradoras do país. Em nosso site, você encontrará um ambiente seguro, fácil e intuitivo para comprar o seguro mais adequado e os meios para esclarecer suas dúvidas.</p>
			</div>
		</div>
	</footer>
</body>
</html>