<?php	
	session_start();	
    
	// login
	if(isset($_POST['send'])){        
		if(($_POST['user'] == 'adm@bemmaisseguro') AND ($_POST['pass'] == '2rswKepw')){
			$_SESSION['acess'] = true;
		}			
	}	
	
	// logout
	if(isset($_POST['logoff'])){						
		session_destroy();			
		header('Location: /admin/');
		exit;
	}
		
	// genetate excel file
	if(isset($_POST['generate'])){				
		include('PHPExcel/Classes/PHPExcel.php');
		include('connect.php');
		
		$objPHPExcel = new PHPExcel();						
		$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getFont()->setBold(true);		
		
		//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, "ID");	
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, "Nome");	
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, "Telefone");	
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, "Email");
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, "Data");
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, "Página");
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, "Observações");
		
		if($_POST['page'] == ''){
			$sql = "SELECT * FROM client;";
		}
		else{
			$sql = "SELECT * FROM client WHERE page = '".$_POST['page']."';";
		}
		
		$qry = mysql_query($sql);									
		$x = 0;
		$y = 2;
		while($row = mysql_fetch_array($qry)){	
			//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(column, line, data);	
			//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++, $y, $row['id']);	
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++, $y, $row['name']);	
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++, $y, $row['phone']);	
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++, $y, $row['email']);			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++, $y, $row['date']);			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++, $y, $row['page']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++, $y, $row['notice']);
			$x = 0;
			$y++;
		}

		$objPHPExcel->getActiveSheet()->setTitle('Relatório');

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Relatorio.xls"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output'); 
		exit;		
	}
?>
<!DOCTYPE html>
<html lang='pt-br'>
<head>
	<meta name='viewport' content='user-scalable=no, initial-scale=1'/>
	<meta charset='utf-8'/>		
	<meta name='author' content='TOPDeals'/>
	<meta name='description' content=''/>
	<meta name='keywords' content=''/>
	<meta property='og:title' content='Administrador'/>
	<meta property='og:type' content='website'/>
	<meta property='og:url' content=''/>
	<meta property='og:image' content=''/>
	<meta property='og:description' content=''/>
	<meta property='fb:app_id' content=''/>
	<link rel='image_src' href=''/>
	<link rel='shortcut icon' type='image/x-icon' href='images/favicon.ico'/>		
	<link href='css/css.css' rel='stylesheet' type='text/css' media='all'/>			
	<title>Relatórios</title>		
</head>
<body>
<img src='images/logo.png' id='logoAdm' alt='logo'/>	
<div class='full'>	
	<?php    
	if(isset($_SESSION['acess'])){			
	?>
		<form name='generate' id='formGenerate' method='POST' action=''>
			<fieldset>	
				<label for='info0'>
					<input type='radio' name='page' value='' checked id='info0'/>
					todos
				</label>
				<label for='info1'>
					<input type='radio' name='page' value='vml smartphone' id='info1'/>
					vml smartphone
				</label>
				<label for='info2'>
					<input type='radio' name='page' value='top smartphone' id='info2'/>
					top smartphone
				</label>
				<label for='info3'>
					<input type='radio' name='page' value='vml viagem' id='info3'/>
					vml viagem
				</label>
				<label for='info4'>
					<input type='radio' name='page' value='top viagem' id='info4'/>
					top viagem
				</label>
				<label for='info5'>
					<input type='radio' name='page' value='top eletrônico' id='info5'/>
					top eletrônico
				</label>
				<label for='info6'>
					<input type='radio' name='page' value='top viagem' id='info6'/>
					top pet
				</label>
				<input type='submit' name='generate' value='GERAR EXCEL'/>
			</fieldset>	
		</form>
		<form name='logout' id='formLogout' method='POST' action=''>
			<input type='submit' name='logoff' value='DESLOGAR'/>
		</form>		
	<?php
	}
	else{
	?>
		<form name='acess' id='formAcess' method='POST' action=''>
			<fieldset>				
				<input type='text' name='user' placeholder='USUÁRIO'/>				
				<input type='password' name='pass' placeholder='SENHA'/>
				<input type='submit' name='send' value='ACESSAR'>
			</fieldset>
		</form>
	<?php		
	}	
	?>
</div>
</body>
</html>